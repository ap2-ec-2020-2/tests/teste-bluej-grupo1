
/**
 * Escreva a descrição da classe Aluno aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public abstract class Aluno
{
    protected String nomeCompleto;
    protected String semestre;
    
    public Aluno(String nome,String semestre){
        this.nomeCompleto = nome;
        this.semestre = semestre;
    }
    
    public abstract String nomeCompleto();
    public abstract String getSemestre();
    
    @Override
    public String toString(){
        return nomeCompleto() + " - " + getSemestre();
    }
}
